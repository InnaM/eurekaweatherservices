package open_weather.Open_weather_service.service;

import open_weather.Open_weather_service.repository.OpenWeather;
import open_weather.Open_weather_service.repository.OpenWeatherRepository;
import open_weather.Open_weather_service.repository.Temperature;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class OpenServiceTest2 {

    @TestConfiguration
    static class OpenServiceTestConfiguration{

        @Bean
        public OpenService openService(OpenWeatherRepository repository, DtoTransformer transformer){
            return new OpenService(repository, transformer);
        }
    }
    @Autowired
    private OpenService service;

    @MockBean
    private OpenWeatherRepository repository;

    @MockBean
    private DtoTransformer transformer;

    @Test
    void should_return_weather_dto (){
        //given
        OpenWeather openWeather = new OpenWeather(new Temperature(27.95, 1005));
        WeatherDTO weatherDTO=new WeatherDTO(27.95, 1005);
        when(repository.getForObject("tokyo")).thenReturn(openWeather);
        when(transformer.weatherToDTO(openWeather)).thenReturn(weatherDTO);

        //when
        WeatherDTO result=service.getWeatherDTO("tokyo");
        //then
        assertEquals(weatherDTO.getTemp(), result.getTemp());
        assertEquals(weatherDTO, result);
    }
}
