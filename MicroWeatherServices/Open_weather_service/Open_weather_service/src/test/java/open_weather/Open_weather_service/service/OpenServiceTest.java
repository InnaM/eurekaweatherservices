package open_weather.Open_weather_service.service;

import open_weather.Open_weather_service.repository.OpenWeather;
import open_weather.Open_weather_service.repository.OpenWeatherRepository;
import open_weather.Open_weather_service.repository.Temperature;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class OpenServiceTest {

    @Mock
    private OpenWeatherRepository repository;

    @Mock
    private DtoTransformer transformer;

    @InjectMocks
    private OpenService service;

    @Test
    void should_return_weather_dto() {
        //given
        OpenWeather openWeather = new OpenWeather(new Temperature(27.95, 1005));
        WeatherDTO weatherDTO=new WeatherDTO(27.95, 1005);
        Mockito.when(repository.getForObject("tokyo")).thenReturn(openWeather);
        Mockito.when(transformer.weatherToDTO(openWeather)).thenReturn(weatherDTO);
        //when
        WeatherDTO result = service.getWeatherDTO("tokyo");

        //then
        assertEquals(weatherDTO.getTemp(), result.getTemp());
        assertEquals(weatherDTO.getPressure(), result.getPressure());
    }
}