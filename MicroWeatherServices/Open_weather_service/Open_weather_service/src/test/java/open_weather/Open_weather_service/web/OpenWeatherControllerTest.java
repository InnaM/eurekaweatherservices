package open_weather.Open_weather_service.web;


import open_weather.Open_weather_service.service.OpenService;
import open_weather.Open_weather_service.service.WeatherDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@ExtendWith(SpringExtension.class)
@WebMvcTest(OpenWeatherController.class)
class OpenWeatherControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OpenService service;

    @Test
    void when_send_get_by_city_request_should_return_weather_dto() throws Exception {
        //given
        when(service.getWeatherDTO("tokyo")).thenReturn(new WeatherDTO(27.95, 1005));
        //when
        //then

        mvc.perform(get("/open/tokyo").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.temp", equalTo(27.95)))
                .andExpect(jsonPath("$.pressure", equalTo(1005)));
    }
}