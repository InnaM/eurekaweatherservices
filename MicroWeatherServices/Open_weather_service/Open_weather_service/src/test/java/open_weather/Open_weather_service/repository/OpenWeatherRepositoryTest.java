package open_weather.Open_weather_service.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OpenWeatherRepositoryTest {
    @Mock
    private RestTemplate openWeatherTemplate;

    @InjectMocks
    private OpenWeatherRepository repository;

    @Test
    void should_return_open_weather_from_url(){
        //given
        OpenWeather openWeather = new OpenWeather(new Temperature(27.95, 1005));
        when(openWeatherTemplate.getForObject("http://api.openweathermap.org/data/2.5/weather?q=tokyo&appid=baa6ece140be0985d8bf8766fa381d1d&units=metric",OpenWeather.class ))
                .thenReturn(openWeather);
        //when
        OpenWeather result= repository.getForObject("tokyo");
        //then
        assertEquals(openWeather.getTemperature(), result.getTemperature());
        assertEquals(openWeather.getTemperature().getPressure(), result.getTemperature().getPressure());
    }
}