package open_weather.Open_weather_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class OpenWeatherServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenWeatherServiceApplication.class, args);
    }
}
