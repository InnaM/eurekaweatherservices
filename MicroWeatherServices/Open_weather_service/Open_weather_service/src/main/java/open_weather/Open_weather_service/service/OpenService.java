package open_weather.Open_weather_service.service;

import open_weather.Open_weather_service.repository.OpenWeather;
import open_weather.Open_weather_service.repository.OpenWeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OpenService {
    private final OpenWeatherRepository openWeatherRepository;
    private final DtoTransformer dtoTransformer;

    @Autowired
    public OpenService(OpenWeatherRepository openWeatherRepository, DtoTransformer dtoTransformer) {
        this.openWeatherRepository = openWeatherRepository;
        this.dtoTransformer = dtoTransformer;
    }

    public WeatherDTO getWeatherDTO(String city){
        OpenWeather openWeather=openWeatherRepository.getForObject(city);
        WeatherDTO weatherDTO=dtoTransformer.weatherToDTO(openWeather);
        return weatherDTO;
    }
}
