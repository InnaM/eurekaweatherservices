package open_weather.Open_weather_service.web;

import open_weather.Open_weather_service.service.OpenService;
import open_weather.Open_weather_service.service.WeatherDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/open")
public class OpenWeatherController {

    private OpenService service;

    public OpenWeatherController(OpenService service) {
        this.service = service;
    }

    @GetMapping("/{city}")
    public WeatherDTO getWeatherByCity(@PathVariable String city){
        return service.getWeatherDTO(city);
    }

}
