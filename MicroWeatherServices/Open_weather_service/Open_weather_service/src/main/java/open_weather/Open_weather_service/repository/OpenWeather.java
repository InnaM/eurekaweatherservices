package open_weather.Open_weather_service.repository;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class OpenWeather {

    @JsonProperty ("main")
    private Temperature temperature;

    public OpenWeather(Temperature temperature) {
        this.temperature = temperature;
    }

    public OpenWeather() {
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpenWeather that = (OpenWeather) o;
        return Objects.equals(temperature, that.temperature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature);
    }

    @Override
    public String toString() {
        return "OpenWeather{" +
                "temperature=" + temperature +
                '}';
    }
}
