package open_weather.Open_weather_service.repository;

import java.util.Objects;

public class Temperature {
    private double temp;
    private int pressure;

    public Temperature(double temp, int pressure) {
        this.temp = temp;
        this.pressure = pressure;
    }

    public Temperature() {
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Temperature that = (Temperature) o;
        return Double.compare(that.temp, temp) == 0 && pressure == that.pressure;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temp, pressure);
    }
}
