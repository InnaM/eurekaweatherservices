package open_weather.Open_weather_service.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class OpenWeatherComponentProvider {

    @Bean(name="openWeatherTemplate")
    RestTemplate openWeatherTemplate() {
        return new RestTemplate();
    }
}
