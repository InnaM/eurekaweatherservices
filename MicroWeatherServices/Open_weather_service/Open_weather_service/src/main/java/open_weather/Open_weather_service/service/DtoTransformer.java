package open_weather.Open_weather_service.service;

import open_weather.Open_weather_service.repository.OpenWeather;
import org.springframework.stereotype.Component;

@Component
public class DtoTransformer {

    public WeatherDTO weatherToDTO(OpenWeather openWeather) {
        WeatherDTO weatherDTO = new WeatherDTO();
        weatherDTO.setTemp(openWeather.getTemperature().getTemp());
        weatherDTO.setPressure(openWeather.getTemperature().getPressure());
        return weatherDTO;
    }
}
