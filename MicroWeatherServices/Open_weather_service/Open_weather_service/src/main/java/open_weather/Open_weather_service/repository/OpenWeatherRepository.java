package open_weather.Open_weather_service.repository;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class OpenWeatherRepository {
    private final RestTemplate openWeatherTemplate;

    public OpenWeatherRepository(RestTemplate openWeatherTemplate) {
        this.openWeatherTemplate = openWeatherTemplate;
    }

    public OpenWeather getForObject(String city) {
        String url = String.format(
                "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=baa6ece140be0985d8bf8766fa381d1d&units=metric", city);
        OpenWeather openWeather = openWeatherTemplate.getForObject(url, OpenWeather.class);
        return openWeather;
    }
}
