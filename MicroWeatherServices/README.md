Technologies:
Java 8 
Spring Boot 2.5.2 
Spring Cloud

Description:
Main goal: the api retrieves weather data by city from several microservices and return average values.

The api use Eureka Server and 3 microservices:

1. Open_weather_service
2. Stack_weather_service
3. Weather_service

1. Open_weather_service is available in url: http://localhost:8050/open/{city}
   The microservice retrieves weather dates from url: http://api.openweathermap.org/data/2.5/weather

2. Stack_weather_service is available in url: http://localhost:8060/stack_weather/{city}
   The microservice retrieves weather dates from url: http://api.weatherstack.com/current

3. Weather_service is available in url: http://localhost:8083/weather/{city}
   The microservice retrieves weather dates from previous 2 microservices and return the average values of them.

   {city}= instead of this variable it is necessary to enter your chosen city


   Start app IDE 

To start app, you need download this repository and in your IDE import file pom.xml Now you need run
Discovery Server and main methods of all microservices in your IDE. 
They are available:
1. Discovery Server in: Discovery_server/src/main/java/micro_weather_services.Discovery_server/DiscoveryServerApplication.class
2. Open Weather Service in: Open_weather_service/src/main/java/open_weather.Open_weather_service/OpenWeatherServiceApplication.class
3. Stack Weather Service in: Stack_weather_service/src/main/java/stack_weather.Stack_weather_service/StackWeatherServiceApplication.class
4. Weather Service in: Weather_service/src/main/java/weather_service.Weather_service/WeatherServiceApplication.class