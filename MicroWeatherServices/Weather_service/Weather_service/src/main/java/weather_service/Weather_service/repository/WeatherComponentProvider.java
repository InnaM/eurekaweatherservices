package weather_service.Weather_service.repository;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class WeatherComponentProvider {

    @Bean(name="microServiceTemplate")
    @LoadBalanced
    RestTemplate microServiceTemplate(){
        return new RestTemplate();
    }
}
