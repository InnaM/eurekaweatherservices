package weather_service.Weather_service.repository;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class WeatherRepository {
    private RestTemplate microServiceTemplate;

    public WeatherRepository(RestTemplate microServiceClient) {
        this.microServiceTemplate = microServiceClient;
    }

    public List<WeatherDTO> getWeatherFromServices(String city) {
        List<String> urls = List.of("http://open-weather/open/"+city, "http://stack-weather/stack_weather/"+city);
        return urls.stream().map(url -> microServiceTemplate.getForObject(url, WeatherDTO.class)).collect(Collectors.toList());
    }
}
