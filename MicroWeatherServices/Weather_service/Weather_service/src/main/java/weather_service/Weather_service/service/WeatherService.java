package weather_service.Weather_service.service;

import org.springframework.stereotype.Service;
import weather_service.Weather_service.repository.WeatherRepository;
import weather_service.Weather_service.repository.WeatherDTO;

import java.util.List;

@Service
public class WeatherService {

    private WeatherRepository repository;

    public WeatherService(WeatherRepository repository) {
        this.repository = repository;
    }

    public WeatherDTO getAverageWeather(String city) {
        List<WeatherDTO> weathers = repository.getWeatherFromServices(city);
        WeatherDTO weatherDTO = new WeatherDTO();
        weatherDTO.setTemp(weathers.stream().mapToDouble(w -> w.getTemp()).average().orElse(0));
        double pressure = weathers.stream().mapToDouble((w -> w.getPressure())).average().orElse(0);
        weatherDTO.setPressure((int) pressure);
        return weatherDTO;
    }
}
