package weather_service.Weather_service.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import weather_service.Weather_service.repository.WeatherDTO;
import weather_service.Weather_service.service.WeatherService;

@RestController
@RequestMapping("/weather")
public class WeatherController {
    private WeatherService weatherService;

    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/{city}")
    public WeatherDTO getWeather(@PathVariable String city) {
        return weatherService.getAverageWeather(city);
    }
}
