package weather_service.Weather_service.web;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import weather_service.Weather_service.repository.WeatherDTO;
import weather_service.Weather_service.service.WeatherService;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(WeatherController.class)
class WeatherControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private WeatherService service;

    @Test
    void when_send_get_by_city_request_then_return_average_weather_dto_object() throws Exception {
        //given

        when(service.getAverageWeather("tokyo"))
                .thenReturn(new WeatherDTO(28.53, 1012));
        //when and then
        mvc.perform(get("/weather/tokyo").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.temp", equalTo(28.53)))
                .andExpect(jsonPath("$.pressure", equalTo(1012)))
                .andExpect(status().isOk());
    }
}