package weather_service.Weather_service.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WeatherRepositoryTest {
    @Mock
    private RestTemplate microServiceTemplate;

    @InjectMocks
    private WeatherRepository repository;

    @Test
    void should_return_list_of_weather_dto_from_open_and_stack_services() {
        //given
        WeatherDTO openWeatherDTO = new WeatherDTO(27.95, 1005);
        WeatherDTO stackWeatherDTO = new WeatherDTO(29.0, 1010);
        List<String> urls = List.of("http://open-weather/open/tokyo", "http://stack-weather/stack_weather/tokyo");
        when(microServiceTemplate.getForObject(urls.get(0), WeatherDTO.class)).thenReturn(openWeatherDTO);
        when(microServiceTemplate.getForObject(urls.get(1), WeatherDTO.class)).thenReturn(stackWeatherDTO);
        //when
        List<WeatherDTO> result = repository.getWeatherFromServices("tokyo");
        //then
        assertEquals(List.of(openWeatherDTO, stackWeatherDTO), result);
    }
}