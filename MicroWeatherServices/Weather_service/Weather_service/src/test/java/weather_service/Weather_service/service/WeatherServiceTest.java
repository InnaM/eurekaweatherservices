package weather_service.Weather_service.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import weather_service.Weather_service.repository.WeatherDTO;
import weather_service.Weather_service.repository.WeatherRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class WeatherServiceTest {

    @TestConfiguration
    static class WeatherServiceTestConfiguration{
        @Bean
        public WeatherService weatherService(WeatherRepository repository){
            return new WeatherService(repository);
        }
    }
    @Autowired
    private WeatherService service;

    @MockBean
    private WeatherRepository repository;

    @Test
    void should_get_average_weather_dto_object(){
        //given
        List<WeatherDTO> weathers=List.of(new WeatherDTO(28.06, 1013), new WeatherDTO(29.0, 1012));
        when(repository.getWeatherFromServices("tokyo")).thenReturn(weathers);

        //when
        WeatherDTO result=service.getAverageWeather("tokyo");

        //then
        assertEquals(28.53, result.getTemp());
        assertEquals(1012, result.getPressure());
    }
}