package stack_weather.Stack_weather_service.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stack_weather.Stack_weather_service.service.StackWeatherService;
import stack_weather.Stack_weather_service.service.WeatherDTO;


@RestController
@RequestMapping("/stack_weather")
public class StackWeatherController {
    private StackWeatherService stackWeatherService;

    public StackWeatherController(StackWeatherService stackWeatherService) {
        this.stackWeatherService = stackWeatherService;
    }

    @GetMapping("/{city}")
    public WeatherDTO getWeatherByCity(@PathVariable String city) {
        return stackWeatherService.getWeatherDTO(city);
    }
}
