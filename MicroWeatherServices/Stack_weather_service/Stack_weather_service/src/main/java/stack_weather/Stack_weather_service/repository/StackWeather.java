package stack_weather.Stack_weather_service.repository;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class StackWeather {

    @JsonProperty("current")
    private WeatherInfo weatherInfo;

    public StackWeather(WeatherInfo weatherInfo) {
        this.weatherInfo = weatherInfo;
    }

    public StackWeather() {
    }

    public WeatherInfo getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(WeatherInfo weatherInfo) {
        this.weatherInfo = weatherInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StackWeather that = (StackWeather) o;
        return Objects.equals(weatherInfo, that.weatherInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weatherInfo);
    }

    @Override
    public String toString() {
        return "StackWeather{" +
                "weatherInfo=" + weatherInfo +
                '}';
    }
}
