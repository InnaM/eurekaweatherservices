package stack_weather.Stack_weather_service.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class StackComponentProvider {

    @Bean(name="stackWeatherTemplate")
    RestTemplate stackWeatherTemplate(){
        return new RestTemplate();
    }
}
