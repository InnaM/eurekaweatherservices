package stack_weather.Stack_weather_service.repository;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class StackWeatherRepository {

    private final RestTemplate stackWeatherTemplate;

    public StackWeatherRepository(RestTemplate stackWeatherTemplate) {
        this.stackWeatherTemplate = stackWeatherTemplate;
    }

    public StackWeather getForObject(String city) {
        String url = String.format(
                "http://api.weatherstack.com/current?access_key=17ff09ba764b15dc96f5f8436421b30f&query=%s", city);
        StackWeather stackWeather = stackWeatherTemplate.getForObject(url, StackWeather.class);
        return stackWeather;
    }
}
