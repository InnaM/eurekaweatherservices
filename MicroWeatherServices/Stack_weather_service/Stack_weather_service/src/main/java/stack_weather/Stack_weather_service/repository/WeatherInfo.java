package stack_weather.Stack_weather_service.repository;

import java.util.Objects;

public class WeatherInfo {
    private int temperature;
    private int pressure;

    public WeatherInfo(int temperature, int pressure) {
        this.temperature = temperature;
        this.pressure = pressure;
    }

    public WeatherInfo() { }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherInfo that = (WeatherInfo) o;
        return temperature == that.temperature && pressure == that.pressure;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature, pressure);
    }

    @Override
    public String toString() {
        return "WeatherInfo{" +
                "temperature=" + temperature +
                ", pressure=" + pressure +
                '}';
    }
}
