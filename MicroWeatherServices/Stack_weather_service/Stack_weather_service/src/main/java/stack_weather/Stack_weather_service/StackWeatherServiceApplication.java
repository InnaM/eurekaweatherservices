package stack_weather.Stack_weather_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class StackWeatherServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StackWeatherServiceApplication.class, args);
	}

}
