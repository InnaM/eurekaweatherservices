package stack_weather.Stack_weather_service.service;

import java.util.Objects;

public class WeatherDTO {
    private double temp;
    private int pressure;

    public WeatherDTO(double temp, int pressure) {
        this.temp = temp;
        this.pressure = pressure;
    }

    public WeatherDTO() {
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherDTO that = (WeatherDTO) o;
        return Double.compare(that.temp, temp) == 0 && pressure == that.pressure;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temp, pressure);
    }

    @Override
    public String toString() {
        return "WeatherDTO{" +
                "temp=" + temp +
                ", pressure=" + pressure +
                '}';
    }
}
