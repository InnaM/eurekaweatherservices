package stack_weather.Stack_weather_service.service;

import org.springframework.stereotype.Component;
import stack_weather.Stack_weather_service.repository.StackWeather;

@Component
public class DtoTransformer {

    public WeatherDTO weatherToDTO(StackWeather stackWeather) {
        WeatherDTO weatherDTO = new WeatherDTO();
        weatherDTO.setTemp(stackWeather.getWeatherInfo().getTemperature());
        weatherDTO.setPressure(stackWeather.getWeatherInfo().getPressure());
        return weatherDTO;
    }
}
