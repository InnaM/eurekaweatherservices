package stack_weather.Stack_weather_service.service;

import org.springframework.stereotype.Service;
import stack_weather.Stack_weather_service.repository.StackWeather;
import stack_weather.Stack_weather_service.repository.StackWeatherRepository;

@Service
public class StackWeatherService {
    private final StackWeatherRepository stackWeatherRepository;
    private final DtoTransformer dtoTransformer;

    public StackWeatherService(StackWeatherRepository stackWeatherRepository, DtoTransformer dtoTransformer) {
        this.stackWeatherRepository = stackWeatherRepository;
        this.dtoTransformer = dtoTransformer;
    }

    public WeatherDTO getWeatherDTO(String city) {
        StackWeather stackWeather = stackWeatherRepository.getForObject(city);
        WeatherDTO weatherDTO = dtoTransformer.weatherToDTO(stackWeather);
        return weatherDTO;
    }
}
