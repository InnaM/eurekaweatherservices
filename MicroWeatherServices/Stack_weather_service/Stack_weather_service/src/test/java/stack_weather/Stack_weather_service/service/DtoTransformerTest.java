package stack_weather.Stack_weather_service.service;

import org.junit.jupiter.api.Test;
import stack_weather.Stack_weather_service.repository.StackWeather;
import stack_weather.Stack_weather_service.repository.WeatherInfo;

import static org.junit.jupiter.api.Assertions.*;

class DtoTransformerTest {

    private final DtoTransformer transformer=new DtoTransformer();
    @Test
    void should_return_weather_dto_object(){
        //given
        StackWeather stackWeather=new StackWeather(new WeatherInfo(29, 1012));

        //when
        WeatherDTO result= transformer.weatherToDTO(stackWeather);

        //then
        assertEquals((double)29, result.getTemp());
        assertEquals(1012, result.getPressure());
    }

}