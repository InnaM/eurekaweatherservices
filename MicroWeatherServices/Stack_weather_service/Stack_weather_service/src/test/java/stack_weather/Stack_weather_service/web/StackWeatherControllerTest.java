package stack_weather.Stack_weather_service.web;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import stack_weather.Stack_weather_service.service.StackWeatherService;
import stack_weather.Stack_weather_service.service.WeatherDTO;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@WebMvcTest(StackWeatherController.class)
class StackWeatherControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private StackWeatherService service;

    @Test
    void when_send_get_by_city_request_should_return_weather_dto() throws Exception {
        //given
        when(service.getWeatherDTO("tokyo")).thenReturn(new WeatherDTO(29.0, 1010));

        //when
        //then
        mvc.perform(get("/stack_weather/tokyo").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.temp", equalTo(29.0)))
                .andExpect(jsonPath("$.pressure", equalTo(1010)));
    }
}