package stack_weather.Stack_weather_service.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import stack_weather.Stack_weather_service.repository.StackWeather;
import stack_weather.Stack_weather_service.repository.StackWeatherRepository;
import stack_weather.Stack_weather_service.repository.WeatherInfo;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class StackWeatherServiceTest {

    @TestConfiguration
    static class StackWeatherServiceTestConfiguration {

        @Bean
        public StackWeatherService stackService(StackWeatherRepository repository, DtoTransformer dtoTransformer) {
            return new StackWeatherService(repository, dtoTransformer);
        }
    }
    @Autowired
    private StackWeatherService service;

    @MockBean
    private StackWeatherRepository repository;
    @MockBean
    private DtoTransformer dtoTransformer;

    @Test
    void should_return_weather_dto_object() {
        //given
        StackWeather stackWeather = new StackWeather(new WeatherInfo(29, 1010));
        WeatherDTO weatherDTO= new WeatherDTO(29.0, 1010);
        when(repository.getForObject("tokyo")).thenReturn(stackWeather);
        when(dtoTransformer.weatherToDTO(stackWeather)).thenReturn(weatherDTO);

        //when
        WeatherDTO result=service.getWeatherDTO("tokyo");

        //then
        assertEquals(weatherDTO.getTemp(), result.getTemp());
        assertEquals(weatherDTO.getPressure(), result.getPressure());
    }
}