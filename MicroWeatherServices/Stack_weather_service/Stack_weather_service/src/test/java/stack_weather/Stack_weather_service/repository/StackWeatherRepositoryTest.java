package stack_weather.Stack_weather_service.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StackWeatherRepositoryTest {

    @Mock
    private RestTemplate stackWeatherTemplate;

    @InjectMocks
    private StackWeatherRepository repository;

    @Test
    void should_return_stack_weather_from_url() {
        //given
        StackWeather stackWeather = new StackWeather(new WeatherInfo(29, 1010));
        when(stackWeatherTemplate.getForObject("http://api.weatherstack.com/current?access_key=17ff09ba764b15dc96f5f8436421b30f&query=tokyo", StackWeather.class))
                .thenReturn(stackWeather);
        //when
        StackWeather result = repository.getForObject("tokyo");
        //then
        assertEquals(stackWeather.getWeatherInfo(), result.getWeatherInfo());
        assertEquals(stackWeather.getWeatherInfo().getTemperature(), result.getWeatherInfo().getTemperature());
    }
}